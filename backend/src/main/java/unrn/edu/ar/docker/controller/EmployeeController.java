package unrn.edu.ar.docker.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import unrn.edu.ar.docker.dto.EmployeeDTO;
import unrn.edu.ar.docker.mappers.DepartmentMapper;
import unrn.edu.ar.docker.mappers.EmployeeMapper;
import unrn.edu.ar.docker.model.Department;
import unrn.edu.ar.docker.model.Employee;
import unrn.edu.ar.docker.repository.DepartmentRepository;
import unrn.edu.ar.docker.repository.EmployeeRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.CachePut;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/employee")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class EmployeeController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

	@Autowired
	EmployeeRepository repository;

	@Autowired
	DepartmentRepository departmentRepository;

	@Autowired
    EmployeeMapper mapper;

	@Autowired
    DepartmentMapper departmentMapper;

	@PostMapping("/")
	@CachePut(value = "employees", key = "#employees.id")
	public EmployeeDTO add(@RequestBody EmployeeDTO employeeDTO) {
		LOGGER.info("Employee add: {}", employeeDTO);

		Employee employee = mapper.toEntity(employeeDTO);
		employee = repository.save(employee);
		return mapper.toDTO(employee);
	}

	@GetMapping("/{id}")
	@Cacheable(value = "employees", key = "#id")
	public EmployeeDTO findById(@PathVariable("id") String id) {
		LOGGER.info("Employee find: id={}", id);
		Employee employee = repository.findById(id).orElse(null);
		EmployeeDTO dto = null;

		if (employee != null) {
			Department department = employee.getDepartmentId() != null ?
					departmentRepository.findById(employee.getDepartmentId()).orElse(null) : null ;

			dto = mapper.toDTO(employee);
            dto.setDepartment(departmentMapper.toDTO(department));
        }

		return dto;
	}

	@GetMapping("/")
	@Cacheable(value = "employees")
	public List<EmployeeDTO> findAll() {
		LOGGER.info("Employee find");
		return repository.findAll().stream().map(emp -> mapper.toDTO(emp)).collect(Collectors.toList());
	}


	@GetMapping("/organization/{organizationId}")
	public List<EmployeeDTO> findByOrganization(@PathVariable("organizationId") String organizationId) {
		LOGGER.info("Employee find: organizationId={}", organizationId);
		return repository.findEmployeesByDepartmentId(organizationId)
                .stream().map(emp -> mapper.toDTO(emp)).collect(Collectors.toList());
	}

	@DeleteMapping("/{id}")
    public void deleteEmployee(@PathVariable("id") String id){
        LOGGER.info("Deleting..." + id);
        if (repository.findById(id).isPresent())
            repository.deleteById(id);
    }

}
